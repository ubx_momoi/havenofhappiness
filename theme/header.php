<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta property="og:site_name" content="東京都美術館">
<meta property="og:type" content="website">
<meta property="og:title" content="「楽園としての芸術」展 - Art as a Haven of Happiness 2014年7月26日(土)～10月8日(水) 東京都美術館[上野公園]">
<meta property="og:image" content="http://havenofhappiness.tobikan.jp/images/fb_img.jpg">
<meta name="description" content="">
<title>「楽園としての芸術」展 | Art as a Haven of Happiness</title>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="shortcut icon" href="<?php echo home_url(); ?>/favicon.ico">
<?php wp_head(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<!-[if lte IE 9 ]><script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script><![endif]->
</head>
<body>

<?php
	$body_id ="";
	if ( is_page() ) {
		$body_id = $post->post_name;
		if (("interview1" == $body_id) || ("interview2" == $body_id) || ("interview3" == $body_id)) {
			$body_id = "interview";
		}
	} else if ( is_home() ) {
		$body_id = 'site_index';
	} else if ( 'news' == get_post_type() ) {
		$body_id = 'news';
	} else if ( is_single() || is_archive()) {
		$body_id = 'blog';
	} else {
	}
?>

<div id="top"></div>

<div id="wrapper">
<div id="<?php echo $body_id; ?>">

    <!-- ////////// HEADER AREA ////////// -->
	<header class="base_w">
	<?php if($body_id == 'site_index') { ?>
		<nav>
			<ul class="menu clearfix">
				<li class="menu_message"><a href="<?php echo home_url(); ?>/message/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_message_off.png" alt="メッセージ message"></a></li>
				<li class="menu_news"><a href="<?php echo home_url(); ?>/news/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_news_off.png" alt="ニュース news"></a></li>
				<li class="menu_about"><a href="<?php echo home_url(); ?>/about/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_about_off.png" alt="「アトリエ・エレマン・プレザン」と 「しょうぶ学園」 atelier elément présent &amp; shobu gakuen"></a></li>
				<li class="menu_blog"><a href="<?php echo home_url(); ?>/blog/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_blog_off.png" alt="日々是好日 担当学芸員のブログ blog"></a></li>
				<li class="menu_information"><a href="<?php echo home_url(); ?>/information/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_information_off.png" alt="展覧会基本情報 information"></a></li>
			</ul>
		</nav>
	<?php } else { ?>
		<h1 class="ttl"><a href="<?php echo home_url(); ?>" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/ttl.jpg" alt="Art as a Haven of Happiness 「楽園としての芸術」展"></a></h1>
		<nav>
			<ul class="menu clearfix">
		<?php if($body_id == 'message') { ?>
				<li class="menu_message"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_message_on.png" alt="メッセージ message"></li>
		<?php } else { ?>
				<li class="menu_message"><a href="<?php echo home_url(); ?>/message/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_message_off.png" alt="メッセージ message"></a></li>
		<?php } ?>
		<?php if($body_id == 'news') { ?>
				<li class="menu_news"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_news_on.png" alt="ニュース news"></li>
		<?php } else { ?>
				<li class="menu_news"><a href="<?php echo home_url(); ?>/news/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_news_off.png" alt="ニュース news"></a></li>
		<?php } ?>
		<?php if($body_id == 'about') { ?>
				<li class="menu_about"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_about_on.png" alt="「アトリエ・エレマン・プレザン」と 「しょうぶ学園」 atelier elément présent &amp; shobu gakuen"></li>
		<?php } else { ?>
				<li class="menu_about"><a href="<?php echo home_url(); ?>/about/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_about_off.png" alt="「アトリエ・エレマン・プレザン」と 「しょうぶ学園」 atelier elément présent &amp; shobu gakuen"></a></li>
		<?php } ?>
		<?php if($body_id == 'blog') { ?>
				<li class="menu_blog"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_blog_on.png" alt="日々是好日 担当学芸員のブログ blog"></li>
		<?php } else { ?>
				<li class="menu_blog"><a href="<?php echo home_url(); ?>/blog/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_blog_off.png" alt="日々是好日 担当学芸員のブログ blog"></a></li>
		<?php } ?>
		<?php if($body_id == 'information') { ?>
				<li class="menu_information"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_information_on.png" alt="展覧会基本情報 information"></li>
		<?php } else { ?>
				<li class="menu_information"><a href="<?php echo home_url(); ?>/information/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_information_off.png" alt="展覧会基本情報 information"></a></li>
		<?php } ?>
			</ul>
		</nav>
	<?php } ?>
	</header>

    <!-- ////////// HEADER AREA ////////// -->

    <!-- ////////// CONTENTS AREA ////////// -->
	<div id="contents">
