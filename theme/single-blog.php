<?php get_header(); ?>

<div class="contents_bg mt45">
	<div class="base_w">
		<h2 class="ml10"><img src="<?php echo get_template_directory_uri(); ?>/images/blog_ttl.png" alt="日々是好日 担当学芸員のブログ 【blog】"></h2>
		<div class="clearfix mt60">
			<aside class="blog_l ml10">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/images/categories_ttl.jpg" alt="カテゴリー categories"></h3>
				<ul class="side_list mt10">
					<?php
						$cat_all = get_terms( "category", "fields=all&get=all" );
    					foreach($cat_all as $value):
    				?>
					<li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name;?></a>&nbsp;(<?php echo $value->count;?>)</li>
					<?php
						endforeach;
					?>
				</ul>
				<h3 class="mt40"><img src="<?php echo get_template_directory_uri(); ?>/images/archives_ttl.jpg" alt="アーカイブ archives"></h3>
				<ul class="side_list mt10">
					<?php wp_get_archives('type=monthly&show_post_count=true'); ?>
				</ul>
			</aside>
			<?php if (have_posts()) : ?>
			<div class="blog_r">
				<ul>
					<?php while (have_posts()) : the_post(); ?>
					<li>
						<?php /* ▼投稿日時を表示させます */ ?>
						<div class="date"><span class="date_bg"><?php echo get_the_date("Y") ?><span class="small">年</span> <?php echo get_the_date("n") ?><span class="small">月</span><?php echo get_the_date("d") ?><span class="small">日</span></span></div>
						<div class="read">
							<?php /* ▼投稿で書いた記事タイトルや本文が入ります */ ?>
							<?php /* the_title(); */ ?>

							<?php /* ▼記事本文を表示させます */ ?>
							<?php the_content(); ?>

							<?php /* ▼小カテゴリ表示 */ ?>
							<p class="cat_name">categories：
							<?php
							$cats = get_the_category();
							foreach($cats as $cat) {
							    if($cat->category_parent != 0) {
									echo "<a href=". get_category_link($cat->cat_ID). ">" .$cat->cat_name ."</a>&nbsp;";
							    }
							}
							?>
							</p>
						</div>
					</li>
					<?php endwhile; ?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<ul class="bnr mt60">
	<li class="mr25"><a hreF="http://www.element-present.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bnr_atelier_element_present banner.jpg" alt="アトリエ・エレマン・プレザン" ></a><span>アトリエ・エレマン・プレザン</span></li>
	<li class="ml25"><a hreF="http://www.shobu.jp" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bnr_shobu_gakuen_banner.jpg" alt="しょうぶ学園"></a><span>しょうぶ学園</span></li>
</ul>

<?php get_footer(); ?>