<?php
////////////////////////////////////
// ラベル名変更
////////////////////////////////////
add_filter(  'gettext',  'change_side_text'  );
add_filter(  'ngettext',  'change_side_text'  );
function change_side_text( $translated ) {
     $translated = str_ireplace(  'ダッシュボード',  '管理画面TOP',  $translated );
     $translated = str_ireplace(  '投稿',  'ブログ',  $translated );
     return $translated;
}
////////////////////////////////////
// カスタム投稿タイプの追加（ニュース）
////////////////////////////////////
add_action( 'init', 'create_post_type' );
function create_post_type() {
  register_post_type( 'news', /* post-type */
    array(
      'labels' => array(
        'name' => __( 'ニュース' ),
        'singular_name' => __( 'ニュース' ),
        'add_new' => __('ニュースの新規追加'),
        'add_new_item' => __('ニュースの新規追加'),
        'edit_item' => __('ニュースを編集'),
        'new_item' => __('新規項目'),
        'view_item' => __('新規追加分を表示'),
        'search_items' => __('News内を検索'),
        'not_found' =>  __('投稿が見つかりません'),
        'not_found_in_trash' => __('ゴミ箱に投稿はありません'),
        'parent_item_colon' => ''
      ),
      'public' => true,
      'has_archive' => true,
      'menu_position' =>5,
    )
  );
}
//投稿時のメッセージとか
add_filter('post_updated_messages', 'news_updated_messages');
function news_updated_messages( $messages ) {
  $messages['news'] = array(
    0 => '', // ここは使用しません
    1 => sprintf( __('ニュースを更新しました <a href="%s">記事を見る</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('カスタムフィールドを更新しました'),
    3 => __('カスタムフィールドを削除しました'),
    4 => __('ニュース更新'),
    5 => isset($_GET['revision']) ? sprintf( __(' %s 前にニュースを保存しました'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('ニュースが公開されました <a href="%s">記事を見る</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('ニュース記事を保存'),
    8 => sprintf( __('ニュース記事を送信 <a target="_blank" href="%s">プレビュー</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('ニュースを予約投稿しました: <strong>%1$s</strong>. <a target="_blank" href="%2$s">プレビュー</a>'),
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('ニュースの下書きを更新しました <a target="_blank" href="%s">プレビュー</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
////////////////////////////////////
// ショートコード
////////////////////////////////////
function shortcode_url() {
 return get_bloginfo('url');
}
add_shortcode('url', 'shortcode_url');
function shortcode_template_url() {
 return "http://havenofhappiness.tobikan.jp/wordpress/wp-content/themes/havenofhappiness/";
}
add_shortcode('template_url', 'shortcode_template_url');
////////////////////////////////////
// <p>タグの表示制御
////////////////////////////////////
//remove_filter('the_content', 'wpautop');
////////////////////////////////////
// <br>タグの表示制御
////////////////////////////////////
/*
function noautop( $content ) {
	if ( strpos( $content, '<!--noautop-->' ) !== false ) {
		remove_filter( 'the_content', 'wpautop' );
		$content = preg_replace( "/\s*\<!--noautop-->\s*(\r\n|\n|\r)?/u", "", $content );
	}
	return $content;
}
add_filter( 'the_content', 'noautop', 1 );
*/
////////////////////////////////////
// ページ送り
////////////////////////////////////
function getPagination($len = 9){
  $len2 = intval($len/2);
  global $paged, $wp_query;
  $now = $paged;// 現在のページ
  $max = $wp_query->max_num_pages;// 総ページ数
  if( empty($now) ){ $now = 1;}
  if( empty($max) ){ $max = 1;}
  if( $max > 1 ){
    $after = 0;
    $before = 0;
    $start = $now - $len2;
    $end = $now + $len2;
    if( $start < 1 ){ $after = 1 - $start;}
    if( $end > $max ){ $before = $end - $max;}
    print('<ul class="nav_page">');
    if( $now-$len2-$before > 1 ){ print('<li class="prev"><a href="'.get_pagenum_link(1).'">&laquo;</a></li>');}
    for($i=1; $i<$max+1; $i++){
      if( $start-$before <= $i && $i <= $end+$after ){
        if( $i == $now ){
          print('<li class="active">'.$i.'</li>');
        } else {
          print('<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>');
        }
      }
    }
    if( $now+$len2+$after < $max ){ print('<li class="next"><a href="'.get_pagenum_link($max).'">&raquo;</a></li>');}
    print('</ul>');
  }
}
////////////////////////////////////
// バージョン更新を非表示にする
////////////////////////////////////
add_filter('pre_site_transient_update_core', '__return_zero');
// APIによるバージョンチェックの通信をさせない
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');
////////////////////////////////////
// フッターWordPressリンクを非表示に
////////////////////////////////////
function custom_admin_footer() {
 echo '<a href="mailto:xxx@zzz">お問い合わせ</a>';
 }
add_filter('admin_footer_text', 'custom_admin_footer');
////////////////////////////////////
// 管理バーの項目を非表示
////////////////////////////////////
function remove_admin_bar_menu( $wp_admin_bar ) {
 $wp_admin_bar->remove_menu( 'wp-logo' ); // WordPressシンボルマーク
 $wp_admin_bar->remove_menu('my-account'); // マイアカウント
 }
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 70 );
////////////////////////////////////
// 管理バーのヘルプメニューを非表示にする
////////////////////////////////////
function my_admin_head(){
 echo '<style type="text/css">#contextual-help-link-wrap{display:none;}</style>';
 }
add_action('admin_head', 'my_admin_head');
////////////////////////////////////
// 管理バーにログアウトを追加
////////////////////////////////////
function add_new_item_in_admin_bar() {
 global $wp_admin_bar;
 $wp_admin_bar->add_menu(array(
 'id' => 'new_item_in_admin_bar',
 'title' => __('ログアウト'),
 'href' => wp_logout_url()
 ));
 }
add_action('wp_before_admin_bar_render', 'add_new_item_in_admin_bar');
////////////////////////////////////
// ダッシュボードウィジェット非表示
////////////////////////////////////
function example_remove_dashboard_widgets() {
 if (!current_user_can('level_10')) { //level10以下のユーザーの場合ウィジェットをunsetする
 global $wp_meta_boxes;
// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
 unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
 unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
 }
 }
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');
////////////////////////////////////
// 投稿画面の項目を非表示にする
////////////////////////////////////
function remove_default_post_screen_metaboxes() {
 if (!current_user_can('level_10')) { // level10以下のユーザーの場合メニューをremoveする
 remove_meta_box( 'postcustom','post','normal' ); // カスタムフィールド
 remove_meta_box( 'postexcerpt','post','normal' ); // 抜粋
 remove_meta_box( 'commentstatusdiv','post','normal' ); // ディスカッション
 remove_meta_box( 'commentsdiv','post','normal' ); // コメント
 remove_meta_box( 'trackbacksdiv','post','normal' ); // トラックバック
 //remove_meta_box( 'authordiv','post','normal' ); // 作成者
 remove_meta_box( 'slugdiv','post','normal' ); // スラッグ
 remove_meta_box( 'revisionsdiv','post','normal' ); // リビジョン
 }
 }
add_action('admin_menu','remove_default_post_screen_metaboxes');
////////////////////////////////////
// 管理画面サイドバーメニュー非表示
////////////////////////////////////
function remove_menus () {
    if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
    global $menu;
//    unset($menu[2]);//ダッシュボード
    unset($menu[4]);//メニューの線1
//    unset($menu[5]);//投稿
//    unset($menu[10]);//メディア
    unset($menu[15]);//リンク
//    unset($menu[20]);//ページ
    unset($menu[25]);//コメント
    unset($menu[59]);//メニューの線2
    unset($menu[60]);//テーマ
    unset($menu[65]);//プラグイン
    unset($menu[70]);//プロフィール
    unset($menu[75]);//ツール
    unset($menu[80]);//設定
    unset($menu[90]);//メニューの線3
    }
}
add_action('admin_menu', 'remove_menus');
?>