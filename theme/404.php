<?php get_header(); ?>

<div class="err_404">
    <h2>指定されたページは存在しませんでした。</h2>
    <p class="url">URL ：<span class="error_msg">http://<?php echo esc_html($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']); ?></span></p>
    <p class="mt20"><a href="<?php echo home_url(); ?>" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/back_top_off.png" alt="トップページへ"></a></p> 
</div>

<?php get_footer(); ?>