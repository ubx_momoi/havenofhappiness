<?php get_header(); ?>
	<?php if (have_posts()) : ?>

<div class="contents_bg mt45">
	<div class="base_w">
		<h2 class="ml10"><img src="<?php echo get_template_directory_uri(); ?>/images/news_ttl.png" alt="ニュース 【news】"></h2>
		<dl class="news_list clearfix">
			<?php $wk_date = ""; ?>
			<?php while (have_posts()) : the_post(); ?>
				<?php
					$str_date = get_the_date("Y年n月");
					if ($wk_date != $str_date){
				?>
				<dt class="date_bg mt50"><?php echo $str_date ?></dt>
				<dd class="mt50">&nbsp;<?php the_time("n月j日"); ?>&nbsp;&nbsp;<?php remove_filter ('the_content', 'wpautop'); ?><?php the_content(); ?></dd>
				<?php
					$wk_date = $str_date;
					} else {
				?>
				<dt>&nbsp;</dt>
				<dd>&nbsp;<?php the_time("n月j日"); ?>&nbsp;&nbsp;<?php remove_filter ('the_content', 'wpautop'); ?><?php the_content(); ?></dd>
				<?php
					}
				?>
			<?php endwhile; ?>
		</dl>
	</div>
</div>
<ul class="bnr mt60">
	<li class="mr25"><a hreF="http://www.element-present.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bnr_atelier_element_present banner.jpg" alt="アトリエ・エレマン・プレザン" ></a><span>アトリエ・エレマン・プレザン</span></li>
	<li class="ml25"><a hreF="http://www.shobu.jp" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bnr_shobu_gakuen_banner.jpg" alt="しょうぶ学園"></a><span>しょうぶ学園</span></li>
</ul>

	<?php endif; ?>
<?php get_footer(); ?>