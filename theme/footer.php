<?php
    $body_id ="";
    if ( is_page() ) {
        $body_id = $post->post_name;
        if (("interview1" == $body_id) || ("interview2" == $body_id) || ("interview3" == $body_id)) {
            $body_id = "interview";
        }
    } else if ( is_home() ) {
        $body_id = 'site_index';
    } else if ( 'news' == get_post_type() ) {
        $body_id = 'news';
    } else if ( is_single() || is_archive()) {
        $body_id = 'blog';
    } else {
        $body_id = 'err';
    }
?>


    </div><!--/contents-->
    <!-- ////////// CONTANTS AREA ////////// -->

    <!-- ////////// FOTTER AREA ////////// -->
    <footer class="base_w">
<?php if($body_id != 'site_index') { ?>
        <p class="page_top"><a href="#top"><img src="<?php echo get_template_directory_uri(); ?>/images/pagetop.png" alt="page_top"></a></p>
<?php } ?>
<?php if($body_id != 'err') { ?>
        <nav>
            <ul class="menu clearfix">
                <?php if($body_id == 'message') { ?>
                <li class="menu_message"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_message_on.png" alt="メッセージ message"></li>
                <?php } else { ?>
                <li class="menu_message"><a href="<?php echo home_url(); ?>/message/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_message_off.png" alt="メッセージ message"></a></li>
                <?php } ?>
                <?php if($body_id == 'news') { ?>
                <li class="menu_news"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_news_on.png" alt="ニュース news"></li>
                <?php } else { ?>
                <li class="menu_news"><a href="<?php echo home_url(); ?>/news/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_news_off.png" alt="ニュース news"></a></li>
                <?php } ?>
                <?php if($body_id == 'about') { ?>
                <li class="menu_about"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_about_on.png" alt="「アトリエ・エレマン・プレザン」と 「しょうぶ学園」 atelier elément présent &amp; shobu gakuen"></li>
                <?php } else { ?>
                <li class="menu_about"><a href="<?php echo home_url(); ?>/about/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_about_off.png" alt="「アトリエ・エレマン・プレザン」と 「しょうぶ学園」 atelier elément présent &amp; shobu gakuen"></a></li>
                <?php } ?>
                <?php if($body_id == 'blog') { ?>
                <li class="menu_blog"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_blog_on.png" alt="日々是好日 担当学芸員のブログ blog"></li>
                <?php } else { ?>
                <li class="menu_blog"><a href="<?php echo home_url(); ?>/blog/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_blog_off.png" alt="日々是好日 担当学芸員のブログ blog"></a></li>
                <?php } ?>
                <?php if($body_id == 'information') { ?>
                <li class="menu_information"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_information_on.png" alt="展覧会基本情報 information"></li>
                <?php } else { ?>
                <li class="menu_information"><a href="<?php echo home_url(); ?>/information/" class="no_opa"><img src="<?php echo get_template_directory_uri(); ?>/images/menu_information_off.png" alt="展覧会基本情報 information"></a></li>
                <?php } ?>
            </ul>
        </nav>
<?php } ?>
        <div class="footer_btm">
            <p class="ml20"><a href="http://www.tobikan.jp/support/about.html" target="_blank">→このサイトについて</a>　　<a href="http://www.tobikan.jp/en/support/about.html" target="_blank">→About This Site</a></p>
            <p class="copyright ml20 mt10">&copy; 2014 TOKYO METROPOLITAN ART MUSEUM. All Rights Reserved.</p>
            <p class="logo"><a href="http://www.tobikan.jp/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.jpg" alt="東京都美術館 TOKYO METROPOLITAN ART MUSEUM"></a></p>
        </div>
    </footer>
    <!-- ////////// FOTTER AREA ////////// -->

</div>
</div><!--/wrapper-->

<!--[if lte IE 6]><script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/DD_belatedPNG.js"></script>
<script type="text/javascript">$(function(){$(window).load(function(){DD_belatedPNG.fix("img,.png_bg");});});</script><![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16014188-2', 'tobikan.jp');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>

<?php wp_footer(); ?>
</body>
</html>
