<?php get_header(); ?>

        <div id="mv">
            <div class=" base_w">
                <div class="social"></div><!--SOCIAL_BUTTON-->
                <h1 class="main_ttl"><img src="<?php echo get_template_directory_uri(); ?>/images/main_ttl.png" alt="Art as a Haven of Happiness 「楽園としての芸術」展  2014年 7月26日（土） - 10月 8日（水） 東京都美術館 [上野公園]"></h1>
                <p class="copyright"><img src="<?php echo get_template_directory_uri(); ?>/images/copyright.png" alt="&copy; Atelier Elément Présent"></p>
            </div>
        </div>

        <div class=" base_w">

            <div id="countdown" style="background:url(<?php echo get_template_directory_uri(); ?>/images/counter_bg.png) no-repeat;height:120px;position:relative;" class="mt10">

                <div id="counter_label" style="position:absolute;bottom:10px;left:275px;"><img src="<?php echo get_template_directory_uri(); ?>/images/counter_label1.png"></div>

                <div id="counter" style="position:absolute;top:15px;left:435px;">
                    <div class="number" style="position:absolute;left:0px;width:60px;height:90px;background:url(<?php echo get_template_directory_uri(); ?>/images/counter_num.png) top left no-repeat;"></div>
                    <div class="number" style="position:absolute;left:60px;width:60px;height:90px;background:url(<?php echo get_template_directory_uri(); ?>/images/counter_num.png) top left no-repeat;"></div>
                    <div class="number" style="position:absolute;left:120px;width:60px;height:90px;background:url(<?php echo get_template_directory_uri(); ?>/images/counter_num.png) top left no-repeat;"></div>
                    <div style="position:absolute;top:40px;left:184px;"><img src="<?php echo get_template_directory_uri(); ?>/images/counter_label2.png"></div>
                </div>
            </div>

            <div class="news">
                <h2 class="sti"><img src="<?php echo get_template_directory_uri(); ?>/images/sti_news.jpg" alt="ニュース news"></h2>
                <ul>
                <?php query_posts('post_type=news&showposts=3'); ?>
                <?php if (have_posts()):while(have_posts()):the_post(); ?>
                    <li><span><?php the_time('Y.n.j'); ?></span>&nbsp;&nbsp;<?php remove_filter ('the_content', 'wpautop'); ?><?php the_content(); ?></li>
                <?php endwhile; endif; ?>
                <?php query_posts($query_string); ?>
                </ul>
                <p class="more"><a href="<?php echo home_url(); ?>/news/"><img src="<?php echo get_template_directory_uri(); ?>/images/more_news.jpg" alt="ニュース一覧"></a></p>
            </div>
        </div>

        <div class="interview">
            <div class="base_w">
                <h2 class="sti"><img src="<?php echo get_template_directory_uri(); ?>/images/sti_interview.png" alt="スペシャルインタビュー special interview"></h2>
                <div class="txt">芸術の醍醐味とは、<br><span class="bold">つくり手にも鑑賞者にも「特別な経験」が与えられること。</span><br>国内外で活躍中する、クリエーターの方々が語る、「楽園展」の魅力をご紹介します。</div>
                <ul class="clearfix mt60">
                    <li class="f_l pt10"><a href="<?php echo home_url(); ?>/interview1/"><img src="<?php echo get_template_directory_uri(); ?>/images/interview1.jpg" alt="デザイナー 吉岡徳仁 TOKUJIN  YOSHIOKA"></a></li>
                    <li class="f_l pt10 pl40"><a href="<?php echo home_url(); ?>/interview2/"><img src="<?php echo get_template_directory_uri(); ?>/images/interview2.jpg" alt="イラストレーター 大橋 歩 AYUMI OHASHI"></a></li>
                    <li class="f_l pt10 pl40"><a href="<?php echo home_url(); ?>/interview3/"><img src="<?php echo get_template_directory_uri(); ?>/images/interview3.jpg" alt="ファッションデザイナー 皆川 明 AKIRA　MINAGAWA"></a></li>
                </ul>
            </div>
        </div>

        <div class="message">
            <div class=" base_w">
                <h2 class="sti"><img src="<?php echo get_template_directory_uri(); ?>/images/sti_message.jpg" alt="メッセージ message"></h2>
                <div class="clearfix mt40">
                    <ul class="art">
                        <li class="pl10"><img src="<?php echo get_template_directory_uri(); ?>/images/message_img01_s.jpg" alt="中野圭（アトリエ・エレマン・プレザン） 《いろ》"><div class="pt5">中野圭（アトリエ・エレマン・プレザン）<br>《いろ》 2013年 油彩、アルシュ紙<br>&copy;Atelier Elément Présent</div></li>
                        <li class="pl45"><img src="<?php echo get_template_directory_uri(); ?>/images/message_img02_s.jpg" alt="田中英明（しょうぶ学園） 無題"><div class="pt5">田中英明（しょうぶ学園）<br>無題 1999年　綿糸、綿布<br>&copy;Shobu Gakuen</div></li>
                    </ul>
                    <div class="read ml40">
                        <h3>「楽園としての芸術」展とは？</h3>
                        <p>人の営みにおいて、芸術は本来どのような役割を担うものなのでしょうか？<br>
　本展では、「アトリエ・エレマン・プレザン」（三重、東京）と「しょうぶ学園」（鹿児島）で制作された絵画・立体・刺繍などを紹介します。つくる歓びとともに、何か深い感情で満たされているような造形の数々は、見るものを陶然とさせる魅力を放っています。<br>
　ダウン症などの障害がある本展のつくり手たちは、ひとり黙々と、あるいは談笑しながら、何の気負いもなく作品を手掛けていきます。なかには、筆を執る前とは別人のような集中力を発揮し、目の覚めるような絵をたちま...</p>
                        <div class="more01"><a href="<?php echo home_url(); ?>/message/"><img src="<?php echo get_template_directory_uri(); ?>/images/more01.jpg" alt="もっと見る"></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about">
            <div class=" base_w">
                <h2 class="sti"><img src="<?php echo get_template_directory_uri(); ?>/images/sti_about.png" alt="「アトリエ・エレマン・プレザン」と 「しょうぶ学園」 atelier elément présent &amp; shobu gakuen"></h2>
                <div class="clearfix">
                    <div class="read pl10">
                        <h3>アトリエ・エレマン・プレザン</h3>
                        <div class="block">
                            <div class="block_in"></div>
                            <div class="more02"><a href="<?php echo home_url(); ?>/about/#atelier_element_present"><img src="<?php echo get_template_directory_uri(); ?>/images/more02.jpg" alt="もっと見る"></a></div>
                            <p>1991（平成3）年、英虞（あご）湾を臨む風光明媚な三重県大王町の伊勢志摩国定公園のなかに、画家の佐藤肇氏と佐藤敬子氏夫妻により設立されました。<br>
　「エレマン・プレザン」とは、フランス語で「現代の要素」という意味です。今この時代において、アトリエが重要な構成要素でありたいという願いが込められています。東京では長女の佐藤よし子氏と佐久間寛厚氏による代々木での活動を経たのち、現在は経堂の閑静な住宅地にアトリエがあり、三重と経堂とも、ダウン症の人たちを中心とした絵画制作が行われています。<br>
　作品のスタイルは様々ですが、アトリエで「指導」の手が...</p>
                        </div>
                    </div>
                    <div class="read pl20">
                        <h3>しょうぶ学園</h3>
                        <div class="block">
                            <div class="block_in"></div>
                            <div class="more02"><a href="<?php echo home_url(); ?>/about/#shobu_gakuen"><img src="<?php echo get_template_directory_uri(); ?>/images/more02.jpg" alt="もっと見る"></a></div>
                            <p>1973（昭和48）年、鹿児島県吉野町において、知的障害者援護施設として設立されました。大島紬や刺し子、竹細工などの下請け作業を行っていましたが、現統括施設長と副施設長である福森伸氏・福森順子氏夫妻の考え方を柱に、1985（昭和60）年頃から、障害のあるつくり手が本来もっている個性を最大限に活かせるような、つくる喜びにあふれた制作環境へと変化していきました。<br>
　現在は「工房しょうぶ」の名のもとに、「布の工房」「木の工房（木工）」「土の工房（陶芸）」「紙の工房（和紙）」「絵画、造形のアトリエ」があり、自主性を尊重した活動が活発に行われています。「布の工房」からは、「針一本でひたすら縫い続...</p>
                        </div>
                    </div>
                </div>
                <ul class="art mt30">
                    <li class="pl10"><img src="<?php echo get_template_directory_uri(); ?>/images/about_img01.jpg" alt="冬木陽（アトリエ・エレマン・プレザン） 《あおあおあお》"><div class="pt5">冬木陽（アトリエ・エレマン・プレザン）<br>《あおあおあお》<br>2013年 油彩、アルシュ紙<br>&copy;Atelier Elément Présent</div></li>
                    <li class="pl25"><img src="<?php echo get_template_directory_uri(); ?>/images/about_img02.jpg" alt="岡田伸次(アトリエ・エレマン・プレザン) 《かあちゃんのかみのけがない》"><div class="pt5">岡田伸次 (アトリエ・エレマン・プレザン)<br>《かあちゃんのかみのけがない》<br>2012年 油彩、アルシュ紙<br>&copy;Atelier Elément Présent</div></li>
                    <li class="pl25"><img src="<?php echo get_template_directory_uri(); ?>/images/about_img03.jpg" alt="濱田幹雄(しょうぶ学園) 無題"><div class="pt5">濱田幹雄 (しょうぶ学園)<br>無題<br>2013年　アクリル、和紙<br>&copy;Shobu Gakuen</div></li>
                    <li class="pl25"><img src="<?php echo get_template_directory_uri(); ?>/images/about_img04.jpg" alt="野間口桂介(しょうぶ学園) 無題"><div class="pt5">野間口桂介 (しょうぶ学園)<br>無題<br>2005年　ミクストメディア、綿シャツ<br>&copy;Shobu Gakuen</div></li>
                </ul>
            </div>
        </div>

        <nav class="navi">
            <ul class="base_w">
                <li class="navi_download"><a href="<?php echo home_url(); ?>/wallpaper/"><img src="<?php echo get_template_directory_uri(); ?>/images/navi_download.jpg" alt="壁紙ダウンロード wall paper"></a></li>
                <li class="navi_information"><a href="<?php echo home_url(); ?>/information/"><img src="<?php echo get_template_directory_uri(); ?>/images/navi_information.jpg" alt="展覧会基本情報 information"></a></li>
                <li class="navi_blog"><a href="<?php echo home_url(); ?>/blog/"><img src="<?php echo get_template_directory_uri(); ?>/images/navi_blog.jpg" alt="日々是好日 担当学芸員のブログ blog"></a></li>
            </ul>
        </nav>

        <ul class="bnr mt80">
            <li class="mr25"><a hreF="http://www.element-present.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bnr_atelier_element_present banner.jpg" alt="アトリエ・エレマン・プレザン" ></a><span>アトリエ・エレマン・プレザン</span></li>
            <li class="ml25"><a hreF="http://www.shobu.jp" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/bnr_shobu_gakuen_banner.jpg" alt="しょうぶ学園"></a><span>しょうぶ学園</span></li>
        </ul>

<?php get_footer(); ?>