$(function(){

	var dW = $(window).width();
	var dH = Math.max($(document).height(), $(window).height());

	var browser_name = getBrowser();

	$('#wrapper').fadeIn(1000);

	/* --------------------------------------
	   hover
	--------------------------------------- */
	$('a:not(.no_opa) img').hover(function(){
		$(this).css('opacity','0.8');
	}, function() {
		$(this).css('opacity','1');
	});

	/* --------------------------------------
	   スクロール
	--------------------------------------- */
	$('a[href^=#]').click(function(){
		var speed = 800;
		var href= $(this).attr('href');
		var target = $(href == '#' || href == '' ? 'html' : href);
		var position = target.offset().top;
		$('html, body').animate({scrollTop:position}, speed, 'easeOutQuart');
		return false;
	});

	/* --------------------------------------
	   RollOver
	--------------------------------------- */
	$('a img').hover(function() {
		$(this).attr('src', $(this).attr('src').replace('_off.', '_on.'));
	}, function() {
		$(this).attr('src', $(this).attr('src').replace('_on.', '_off.'));
	});

	/* --------------------------------------
	   IEのPNG画像対策
	--------------------------------------- */
	if(navigator.userAgent.indexOf('MSIE') != -1) {
		$('img').each(function() {
			if($(this).attr('src').indexOf('.png') != -1) {
				$(this).css({
					'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + $(this).attr('src') + '", sizingMethod="scale");'
				});
			}
		});
	}


	/* --------------------------------------
		counter
	--------------------------------------- */
	$("#site_index").each(function(){
		var _cd = $(this).find("#countdown");
		var _label   = _cd.find("#counter_label");
		var _counter = _cd.find("#counter > .number");

		var startday = Math.floor(new Date("2014/7/26").getTime() / 1000 / 60 / 60 / 24);
		var today    = Math.floor(new Date().getTime() / 1000 / 60 / 60 / 24);
		var remain   = startday - today + 1;
		var temp     = 4000;
		var zero     = [0,0,0];


		if(remain > 0)
		{
			var timer = setInterval(function()
			{
				temp += (remain - temp) * 0.1;

				var num = [];
				num[0] = Math.floor( temp % 1000 / 100 );
				num[1] = Math.floor( temp % 100 / 10 );
				num[2] = Math.floor( temp % 10);

				_counter.each(function(i)
				{
					$(this).css({backgroundPosition:"0 " + -90*num[i] + "px"});

					if(i == 0 && temp < 100 && !zero[i])
					{
						zero[i] = 1;
						$(this).parent().animate({left:410},250);
						_label.animate({left:300},250);
						$(this).hide();
					}
					else if(i == 1 && temp < 10 && !zero[i])
					{
						zero[i] = 1;
						$(this).parent().animate({left:380},250);
						_label.animate({left:330},250);
						$(this).hide();
					}
				});
	//			console.log(Math.floor(temp));
				if(Math.floor(temp) == remain)
				{
					clearInterval(timer);
				}
			},33);
		}
		else
		{
			_cd.hide();
		}
	});

	/* --------------------------------------

	--------------------------------------- */
	$(".social").each(function(){

		var _social = $(".social:first");
		if(_social.length > 0)
		{
/*
			if(location.hostname != "tobikan.jp" && location.hostname != "www.tobikan.jp") // -- 140325
			{
				_social.addClass("gray").text("[ ソーシャルボタン：tobikan.jpでのみ表示 ]");
				return false;
			}
*/
			var url = encodeURIComponent(location.href);
			if(url.substr(0,4) == "http")
			{
				var html = '<iframe src="//www.facebook.com/plugins/like.php?href=' + url + '&amp;width=120&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:165px; height:21px;" allowTransparency="true"></iframe>';
				html += '<a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja">　</a>';
				_social.html(html);
				setTimeout(function()
				{
					!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
				},100);
			}
		}
	});


});

/**
 *  ブラウザ名を取得
 *
 *  @return ブラウザ名(ie6、ie7、ie8、ie9、ie10、ie11、chrome、safari、opera、firefox、unknown)
 */

var getBrowser = function(){
	var ua = window.navigator.userAgent.toLowerCase();
	var ver = window.navigator.appVersion.toLowerCase();
	var name = 'unknown';
	if (ua.indexOf("msie") != -1) {
		if (ver.indexOf("msie 6.") != -1) {
			name = 'ie6';
		} else if (ver.indexOf("msie 7.") != -1) {
			name = 'ie7';
		} else if (ver.indexOf("msie 8.") != -1) {
			name = 'ie8';
		} else if (ver.indexOf("msie 9.") != -1) {
			name = 'ie9';
		} else if (ver.indexOf("msie 10.") != -1) {
			name = 'ie10';
		} else{
			name = 'ie';
		}
	} else if(ua.indexOf('trident/7') != -1) {
		name = 'ie11';
	} else if (ua.indexOf('chrome') != -1) {
		name = 'chrome';
	} else if (ua.indexOf('safari') != -1) {
		name = 'safari';
	} else if (ua.indexOf('opera') != -1) {
		name = 'opera';
	} else if (ua.indexOf('firefox') != -1) {
		name = 'firefox';
	}
	return name;
};



